import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-demo-form-sku',
  templateUrl: './demo-form-sku.component.html',
  styleUrls: ['./demo-form-sku.component.css']
})
export class DemoFormSkuComponent implements OnInit {
  myForm: FormGroup;
  productName: string;
  sku: AbstractControl;
  invalidSku;
  constructor(fg: FormBuilder) {
    this.myForm = fg.group({
      'sku': ['', Validators.compose([
        Validators.required, this.sukValidator
      ])],
      'productName': ['', Validators.required]
    });
    this.sku = this.myForm.controls['sku'];
    this.sku.valueChanges.subscribe((value: string) => {
      console.log('sku change to: ', value);
    });

    this.sku.valueChanges.subscribe((form: any) => {
      console.log('form change to: ', form);
    });
  }

  ngOnInit() {
  }
  onSubmit(value: any): void {
    console.log(value);
  }

  sukValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match(/^123/)) {
      return { invalidSku: true };
    }
  }
}
