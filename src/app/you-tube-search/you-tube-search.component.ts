import { Component, OnInit } from '@angular/core';
import { HttpModule } from '@angular/http';
import { YouTubeService, YOUTUBE_API_KEY, YOUTUBE_API_URL } from './youTobeServices';

@Component({
  selector: 'app-you-tube-search',
  templateUrl: './you-tube-search.component.html',
  styleUrls: ['./you-tube-search.component.css']
})
export class YouTubeSearchComponent implements OnInit {
  id: string;
  title: string;
  description: string;
  thumbnailUrl: string;
  videoUrl: string;
  loadingGif: string = ((<any>window).__karma__) ? '' : require('images/loading.gif');
  constructor(obj?: any) {
    this.id = obj && obj.id || null;
    this.title = obj && obj.title || null;
    this.description = obj && obj.description || null;
    this.thumbnailUrl = obj && obj.thumbnailUrl || null;
    this.videoUrl = obj && obj.videoUrl || null;
  }

  ngOnInit() {
  }

}

export const youTubeServiceInjectables: Array<any> = [
  { provide: YouTubeService, useClass: YouTubeService },
  { provide: YOUTUBE_API_KEY, useValue: YOUTUBE_API_KEY },
  { provide: YOUTUBE_API_URL, useValue: YOUTUBE_API_URL }
];
