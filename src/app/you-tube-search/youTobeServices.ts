import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

export let YOUTUBE_API_KEY = 'AIzaSyDOfT_BO81aEZScosfTYMruJobmpjqNeEk';
export let YOUTUBE_API_URL = 'https://www.googleapis.com/youtube/v3/search';


class SearchResult {
    id: string;
    title: string;
    description: string;
    thumbnailUrl: string;
    videoUrl: string;

    constructor(obj?: any) {
        this.id = obj && obj.id || null;
        this.title = obj && obj.title || null;
        this.description = obj && obj.description || null;
        this.thumbnailUrl = obj && obj.thumbnailUrl || null;
        this.videoUrl = obj && obj.videoUrl ||
            `https://www.youtube.com/watch?v=${this.id}`;
    }
}

@Injectable()
export class YouTubeService {
    constructor(
        private http: Http,
        @Inject(YOUTUBE_API_KEY) private apiKey: string,
        @Inject(YOUTUBE_API_URL) private apiUrl: string) {
    }

    // search(query: string): Observable<SearchResult[]> {
    //     const params: string = [
    //         `q=${query}`,
    //         `key=${this.apiKey}`,
    //         `part=snippet`,
    //         `type=video`,
    //         `maxResults=10`
    //     ].join('&');
    //     const queryUrl = `${this.apiUrl}?${params}`;
    //     return this.http.get(queryUrl)
    //         .map((response: Response) => {
    //             return (<any>response.json()).items.map(item => {
    //                 // console.log("raw item", item); // uncomment if you want to debug
    //                 return new SearchResult({
    //                     id: item.id.videoId,
    //                     title: item.snippet.title,
    //                     description: item.snippet.description,
    //                     thumbnailUrl: item.snippet.thumbnails.high.url
    //                 });
    //             });
    //         });
    // }
}
