import { Component, OnInit, ReflectiveInjector, Inject } from '@angular/core';
import { inject } from '../../../../node_modules/@angular/core/testing';
import { ViewPortService } from './ViewPortService';
@Component({
  selector: 'app-di-sample-app',
  templateUrl: './di-sample-app.component.html',
  styleUrls: ['./di-sample-app.component.css']
})
export class DiSampleAppComponent implements OnInit {
  constructor(
    @Inject('ApiServiceAlias') private aliasService: any,
    @Inject('SizeService') private sizeService: any
  ) {
  }

  userInjectors(): void {
    const injector: any = ReflectiveInjector.resolveAndCreate([ViewPortService, {
      provide: 'OtherSizeService',
      useFactory: (viewport: any) => {
        return viewport.determineService();
      },
      deps: [ViewPortService]
    }]);
    const sizeService: any = injector.get('OtherSizeService');
    sizeService.run();
  }

  invokeApi(): void {
    this.aliasService.get();
    this.sizeService.run();
  }

  ngOnInit() {
  }

}
