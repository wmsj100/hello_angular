import { SmallService } from './SmallService';
import { LargeService } from './LargeService';

export class ViewPortService {
    determineService(): any {
        const w: number = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        if (w < 800) {
            return new SmallService();
        } else {
            return new LargeService();
        }
    }
}
