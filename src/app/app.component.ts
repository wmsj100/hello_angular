import { Component } from '@angular/core';
import { ArticleModule } from './article/article.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  articles: ArticleModule[];
  constructor() {
    this.articles = [
      new ArticleModule(
        'Angular 2',
        'http://angular.io',
        3
      ),
      new ArticleModule(
        'Angular 3',
        'http://github.io',
        2
      ),
      new ArticleModule(
        'Angular 4',
        'http://wmsj100.io',
        1
      ),
    ];
  }
  addArticle(title: HTMLInputElement, link: HTMLInputElement): boolean {
    console.log(`Adding article title: ${title.value} and link: ${link.value}`);
    this.articles.push(
      new ArticleModule(title.value, link.value, 0)
    );
    title.value = '';
    link.value = '';
    return false;
  }

  sortedArticles(): ArticleModule[] {
    return this.articles.sort((a: ArticleModule, b: ArticleModule) => b.votes - a.votes);
  }
}
