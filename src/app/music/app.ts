import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { routes } from './router';

@NgModule({
    declarations: [
    ],
    imports: [
        RouterModule.forChild(routes),
    ]
})

export class MusicAppModule {
    constructor() { }
}
