import { Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { AlbumComponent } from './album/album.component';
import { TrackComponent } from './track/track.component';
import { ArtistComponent } from './artist/artist.component';
import { ArticleComponent } from '../article/article.component';
export const routes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'artists', component: ArticleComponent },
    { path: 'albums', component: AlbumComponent },
    { path: 'tracks', component: TrackComponent },
];
