import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { UserItemComponent } from './user-item/user-item.component';
import { UserListComponent } from './user-list/user-list.component';
import { ArticleComponent } from './article/article.component';
import { DemoFormSkuComponent } from './demo-form-sku/demo-form-sku.component';
import { HttpTestComponent } from './http-test/http-test.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '../../node_modules/@angular/common';
import { platformBrowserDynamic } from '../../node_modules/@angular/platform-browser-dynamic';
import { ArtistComponent } from './artist/artist.component';
import { DiSampleAppComponent } from './injector/di-sample-app/di-sample-app.component';
import { ApiService, API_URL } from './injector/di-sample-app/apiService';
import { ViewPortService } from './injector/di-sample-app/ViewPortService';

class MyService {
  getValue(): string {
    return 'a value';
  }
}

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'artists/:id', component: ArtistComponent },
  { path: 'contactus', redirectTo: 'contact' },
];

const isProduction = true;

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    UserItemComponent,
    UserListComponent,
    ArticleComponent,
    DemoFormSkuComponent,
    HttpTestComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    ArtistComponent,
    DiSampleAppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: MyService, useClass: MyService },
    { provide: ApiService, useClass: ApiService },
    ViewPortService,
    { provide: 'ApiServiceAlias', useClass: ApiService },
    {
      provide: 'SizeService', useFactory: (viewport: any) => {
        return viewport.determineService();
      },
      deps: [ViewPortService]
    },
    { provide: API_URL, useValue: isProduction ? 'https://production-api.sample.com' : 'http://dev-api.sample.com' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule).catch((err: any) => console.log(err));
